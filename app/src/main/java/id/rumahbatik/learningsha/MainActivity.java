package id.rumahbatik.learningsha;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class MainActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final EditText editText1 = (EditText) findViewById(R.id.inputText1);
        final EditText editText2 = (EditText) findViewById(R.id.inputText2);
        Button button = (Button) findViewById(R.id.buttonGenerate);
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    TextView textView1 = (TextView) findViewById(R.id.textView1);
                    TextView textView2 = (TextView) findViewById(R.id.textView2);
                    TextView textView3 = (TextView) findViewById(R.id.textView3);
                    String s1 = generateSHA(editText1.getText().toString());
                    String s2 = generateSHA(editText2.getText().toString());
                    textView1.setText(s1);
                    textView2.setText(s2);
                    if (s1.equals(s2))
                        textView3.setText("cocok");
                    else textView3.setText("berbeda");
                    editText2.setText(null);
                    editText1.setText(null);
                }
            });
    }

    String generateSHA(String s) {
        try {
            byte[] bytes = s.getBytes();
            MessageDigest messageDigest = MessageDigest.getInstance("SHA-1");
            messageDigest.reset();
            messageDigest.update(bytes);
            byte digest[] = messageDigest.digest();
            StringBuilder stringBuilder = new StringBuilder();
            for (byte aDigest : digest) {
                stringBuilder.append(
                        Integer.toString(
                                (aDigest & 0xff) + 0x100, 16
                        ).substring(1)
                );
            }
            return stringBuilder.toString();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return null;
        }
    }
}